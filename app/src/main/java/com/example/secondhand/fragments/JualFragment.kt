package com.example.secondhand.fragments

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavArgs
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.secondhand.R
import com.example.secondhand.databinding.FragmentJualBinding
import com.example.secondhand.dialog.CategoryDialogFragment
import com.example.secondhand.model.PreviewDataModel
import com.example.secondhand.viewmodel.AuthViewModel
import com.example.secondhand.viewmodel.CategoryViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import com.example.secondhand.viewmodel.SellerProductViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.koin.android.ext.android.bind
import org.koin.android.ext.android.inject
import java.io.ByteArrayOutputStream
import java.io.File


class JualFragment : Fragment() {
    private var _binding:FragmentJualBinding? = null
    private val binding get() = _binding!!
    private val categoryviewmodel: CategoryViewModel by inject()
    private val sellerproductviewmodel: SellerProductViewModel by inject()
    private val datastoreviewmodel: DataStoreViewModel by inject()
    private val authviewmodel: AuthViewModel by inject()
    private var accesstoken = ""
    private var categoryDetail = ""
    private val name = arrayListOf<String>()
    private val REQUEST_CODE = 200
    private var lokasi = ""
    private var imageUri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentJualBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        val notif = activity?.findViewById<CardView>(R.id.notifNavigation)
        val notifTv = activity?.findViewById<TextView>(R.id.notifNavigationTv)
        val notifX = activity?.findViewById<ImageView>(R.id.notifNavigationX)

        notifX!!.setOnClickListener {
            notif!!.visibility = View.GONE
        }



        binding.ivJualUploadfoto.setOnClickListener {
            openGalleryForImages()
        }

        binding.ivJualArrow.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.tvJualPilihkategori.setOnClickListener {
            categoryviewmodel.getCategoryData.observe(requireActivity()){
                if(it!=null){
                    val fm: FragmentManager? = fragmentManager
                    val tv = CategoryDialogFragment(it)
                    tv.show(fm!!, "Kategori")
                }
            }
        }

        binding.btnJualTerbitkan.setOnClickListener {
            val baos = ByteArrayOutputStream()
            binding.ivJualUploadfoto.drawable?.toBitmap()?.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val imagearray: ByteArray = baos.toByteArray()

            val namaProduk = binding.etJualNamaproduk.text
            val deskripsi = binding.etJualDeskripsi.text
            val harga = binding.etJualHargaproduk.text.toString().replace("-","")
            val kategori = categoryviewmodel.ids.value.toString().replace("[", "").replace("]", "")

            if (namaProduk.isNullOrEmpty()||deskripsi.isNullOrEmpty()||harga.isEmpty()||kategori=="null"||imageUri==null) {
                binding.textInputLayout1.error = "Masukkan nama produk"
                binding.textInputLayout2.error = "Masukkan harga"
                binding.textInputLayout4.error = "Masukkan deskripsi"
                binding.tvJualErrorkategori.visibility = View.VISIBLE
                binding.tvJualErrorfoto.visibility = View.VISIBLE
            }
            else {
                binding.progressBarJual.visibility = View.VISIBLE
                val image = RequestBody.create("image/png".toMediaTypeOrNull(), imagearray)
                val namaProdukBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), namaProduk.toString())
                val deskripsiBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), deskripsi.toString())
                val hargaBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), harga)
                val imageBody = MultipartBody.Part.createFormData("image", "", image)
                val kategoriBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), kategori)
                val lokasibody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "-")
                sellerproductviewmodel.postProduct(accesstoken, namaProdukBody, deskripsiBody, hargaBody, kategoriBody, lokasibody, imageBody)
            }
            sellerproductviewmodel.postProductCode.observe(requireActivity()){
                if (it!=null){
                    when (it){
                        201 -> {
                            lifecycleScope.launchWhenResumed {
                                findNavController().navigateUp()
                                findNavController().navigate(R.id.daftarJualFragment2)
                            }
                            binding.progressBarJual.visibility = View.GONE
                            notifTv?.setText("Produk berhasil diterbitkan.")
                            notif?.visibility = View.VISIBLE
                            sellerproductviewmodel.postProductCode.postValue(null)
                        }
                        400 -> {
                            notifTv?.setText("Maksimal produk yang bisa dijual 5.")
                            binding.progressBarJual.visibility = View.GONE
                            notif?.visibility = View.VISIBLE
                            sellerproductviewmodel.postProductCode.postValue(null)
                        }
                        403-> {
                            notifTv?.setText("Error.")
                            binding.progressBarJual.visibility = View.GONE
                            notif?.visibility = View.VISIBLE
                            sellerproductviewmodel.postProductCode.postValue(null)
                        }
                        503 -> {
                            notifTv?.setText("Layanan tidak tersedia.")
                            binding.progressBarJual.visibility = View.GONE
                            notif?.visibility = View.VISIBLE
                            sellerproductviewmodel.postProductCode.postValue(null)
                        }
                        else -> {
                            notifTv?.setText("Error.${it}")
                            binding.progressBarJual.visibility = View.GONE
                            notif?.visibility = View.VISIBLE
                            sellerproductviewmodel.postProductCode.postValue(null)
                        }
                    }
                }
            }
        }


        binding.btnJualPreview.setOnClickListener {
            val namaProduk = binding.etJualNamaproduk.text.toString()
            val deskripsi = binding.etJualDeskripsi.text.toString()
            val harga = binding.etJualHargaproduk.text.toString().replace("-","")
            var kategori = categoryviewmodel.ids.value.toString().replace("[", "").replace("]", "")
            val image = binding.ivJualUploadfoto.drawable.toBitmap()

            if (namaProduk.isEmpty()||deskripsi.isEmpty()||harga.isEmpty()||kategori=="null"||imageUri==null) {
                binding.textInputLayout1.error = "Masukkan nama produk"
                binding.textInputLayout2.error = "Masukkan harga"
                binding.textInputLayout4.error = "Masukkan deskripsi"
                binding.tvJualErrorkategori.visibility = View.VISIBLE
                binding.tvJualErrorfoto.visibility = View.VISIBLE
            }
            else {
                val previewData = PreviewDataModel(accesstoken, namaProduk, kategori, categoryDetail, harga,  deskripsi, image)
                val action = JualFragmentDirections.actionJualFragment2ToProdukPreviewFragment(previewData)
                it.findNavController().navigate(action)
            }
        }
    }

    private fun observer() {
        datastoreviewmodel.getAccessToken().observe(requireActivity()){
            accesstoken = it
        }

        authviewmodel.getLogin(accesstoken)


        datastoreviewmodel.getUserData().observe(requireActivity()){
            lokasi = it.city.toString()
        }

        categoryviewmodel.ids.observe(requireActivity()){
            val selected = it
            if (!selected.isNullOrEmpty()){
                if (isAdded){
                    categoryviewmodel.getCategoryData.observe(requireActivity()){
                        if (!it.isNullOrEmpty()){
                            name.clear()
                            for (i in 0..(it.size-1)){
                                for (j in 0..(selected.size-1)){
                                    if (selected.elementAt(j) == it.elementAt(i).id)
                                        name.add(it.elementAt(i).name)
                                }
                            }
                            categoryDetail = name.toString()
                        }
                    }
                }
            }
            if (!name.isEmpty()) {
                binding.tvJualPilihkategori.setText(name.toString())
            }
            if (imageUri!=null) {
                binding.ivJualUploadfoto.setImageURI(imageUri)
            }
        }
    }

    private fun openGalleryForImages() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        val mimeTypes = arrayOf("image/jpeg", "image/png", "image/jpg")
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, REQUEST_CODE);
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            if (data?.getData() != null) {
                imageUri = data.data!!
                binding.ivJualUploadfoto.setImageURI(imageUri)
            }
        }
    }
}