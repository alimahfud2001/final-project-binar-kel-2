package com.example.secondhand.dialog

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.lifecycle.lifecycleScope
import com.example.secondhand.R
import com.example.secondhand.databinding.FragmentOpenBidDialogBinding
import com.example.secondhand.model.OrderRequest
import com.example.secondhand.viewmodel.BuyerOrderViewModel
import com.example.secondhand.viewmodel.DataStoreViewModel
import com.example.secondhand.viewmodel.HomeViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class OpenBidDialogFragment : BottomSheetDialogFragment() {

    private var _binding : FragmentOpenBidDialogBinding?=null
    private val binding get() = _binding!!
    private val viewModel: HomeViewModel by inject()
    private val orderViewModel: BuyerOrderViewModel by inject()
    private val dataStoreViewModel: DataStoreViewModel by inject()
    private var accesstoken = ""
    private var id : Int ?= null
    private val _id get() = id!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOpenBidDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataStoreViewModel.getAccessToken().observe(requireActivity()){
            accesstoken = it
        }
        val notif = activity?.findViewById<CardView>(R.id.notifNavigation)
        val notifTv = activity?.findViewById<TextView>(R.id.notifNavigationTv)
        val notifX = activity?.findViewById<ImageView>(R.id.notifNavigationX)
        val mArgs = arguments
        val myValue = mArgs!!.getInt("key")
        viewModel.fetchProductById(myValue,context).observe(viewLifecycleOwner){
            binding.tvTitle.setText(it?.elementAtOrNull(0)?.name)
            binding.tvharga.setText(it?.elementAtOrNull(0)?.basePrice.toString())
            id = it?.elementAtOrNull(0)?.id
        }
        binding.btnTawar.setOnClickListener {
            if (binding.etTawar.text.toString() != ""||!binding.etTawar.text.isNullOrEmpty()){
                val bidPrice = Integer.parseInt(binding.etTawar.text.toString())
                val request = OrderRequest(_id, bidPrice)
                orderViewModel.postBuyerOrder(accesstoken, request)
                orderViewModel.postBuyerOrderCode.observe(viewLifecycleOwner) {
                    if (it != null) {
                        when (it) {
                            201 -> {
                                notifTv?.setText("Harga tawarmu berhasil dikirim ke penjual")
                                notif?.visibility = View.VISIBLE
                                orderViewModel.postBuyerOrderCode.value = null
                                dismiss()
                            }
                            400 -> {
                                notifTv?.setText("Produk sudah limit tawaran")
                                notif?.visibility = View.VISIBLE
                                orderViewModel.postBuyerOrderCode.value = null
                                dismiss()
                            }
                            503 -> {
                                notifTv?.setText("Koneksi Error, Coba lagi nanti")
                                notif?.visibility = View.VISIBLE
                                orderViewModel.postBuyerOrderCode.value = null
                                dismiss()
                            }
                        }
                    }
                }
            }
            else Toast.makeText(requireContext(), "Isi harga tawar", Toast.LENGTH_SHORT).show()
        }
//        orderViewModel.getBuyerOrder.observe(viewLifecycleOwner){
//            val id = it?.elementAtOrNull(0)?.id
//            val status = it?.elementAtOrNull(0)?.status
//        }
//        binding.btnTawar.setOnClickListener{
//            this.dialog?.dismiss()
//        }
        dialog?.setContentView(view)
    }

}